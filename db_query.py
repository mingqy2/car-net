import sqlite3 as sql


def list_components(table):
    msg = ""
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            if table == "Component":
                cur.execute("SELECT * FROM Component")
            elif table == "Fail_mode":
                cur.execute("SELECT * FROM Fail_mode")
            elif table == "Mapping":
                cur.execute("SELECT * FROM Mapping")

            con.commit()
            response = cur.fetchall()
            msg = "done"
    except:
        con.rollback()
        msg = "fail"
    finally:
        print(msg)
        con.close()
        return response


def add_components(table, data):
    msg = ""
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            if table == "Component":
                cur.execute(
                    "INSERT INTO Component (id, component_name,failure_rate, contact, manufacturer) VALUES (?,?,?,?,?)",
                    (data[0], data[1], data[2], data[3], data[4]))
            elif table == "Fail_mode":
                cur.execute("""INSERT INTO Fail_mode (id, fail_mode, fail_code)
                            VALUES (?,?,?)""",
                            (data[0], data[1], data[2]))
            elif table == "Mapping":
                cur.execute("""INSERT INTO Mapping (mapping_id, component_id, fail_mode_id)
                            VALUES (?,?,?)""",
                            (data[0], data[1], data[2]))
            con.commit()
            msg = "done"
    except:
        con.rollback()
        msg = "fail"
    finally:
        con.close()
        return msg


def update_components(table, data):
    msg = ""
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            if table == "Component":
                cur.execute("""UPDATE Component SET id=?,
                            component_name=?, 
                            failure_rate=?, 
                            contact=?,
                            manufacturer=?
                            WHERE num = ?""",
                            (data[0], data[1], data[2], data[3], data[4], data[5]))
            elif table == "Fail_mode":
                cur.execute("""UPDATE Fail_mode SET id=?, 
                            fail_mode=?, 
                            fail_code=?
                            WHERE num=?""",
                            (data[0], data[1], data[2], data[3]))
            elif table == "Mapping":
                cur.execute("""UPDATE Mapping SET mapping_id=?, 
                            component_id=?, 
                            fail_mode_id=?
                            WHERE num=?""",
                            (data[0], data[1], data[2], data[3]))
            con.commit()
            msg = "done"
    except:
        con.rollback()
        msg = "fail"
    finally:
        con.close()
        return msg


def delete_components(table, data):
    msg = ""
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            if table == "Component":
                cur.execute("""DELETE FROM Component WHERE id=?""",
                            (data[0],))
                cur.execute("""DELETE FROM Mapping WHERE component_id=?""",
                            (data[0],))
            elif table == "Fail_mode":
                cur.execute("""DELETE FROM Fail_Mode WHERE id=?""",
                            (data[0],))
                cur.execute("""DELETE FROM Mapping WHERE fail_mode_id=?""",
                            (data[0],))
            elif table == "Mapping":
                cur.execute("""DELETE FROM Mapping WHERE mapping_id=?""",
                            (data[0],))

            con.commit()
            msg = "done"
    except:
        con.rollback()
        msg = "fail"
    finally:
        con.close()
        return msg
