# Model Details
## Model 1
batch size = 10
epochs = 10
Test loss = 0.111
Test accuracy = 0.981

## Model 2
batch size = 32
epochs = 50
Test loss = 0.095
Test accuracy = 0.961

## Model 3(add maxpooling)
batch size = 32
epochs = 50
Test loss = 0.071
Test accuracy = 0.985

## Model 4(add maxpooling)
batch size = 10
epochs = 10
Test loss = 0.0687
Test accuracy = 0.9816

## Model 5(add maxpooling)
batch size = 25
epochs = 20
Test loss = 0.1143
Test accuracy = 0.9726

# Usage
## Option 1
Install all dependencies then pull the gitlab repo and run app.py.

## Option 2 (Recommended)
Install docker then pull the docker image 

`sudo docker pull dinvincible98/car-net`

Then run

`sudo docker run -p 5000:5000 dinvincible98/car-net`   

