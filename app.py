import time
from flask import Flask, render_template, request, make_response, jsonify,flash,redirect,url_for,send_from_directory
from tensorflow.python.ops.gen_math_ops import mod
import db_query
import json
from src import sort
from src import model
from src import gen_data
from src import test_model
from werkzeug.utils import secure_filename
import os
import numpy as np




UPLOAD_FOLDER = './Upload'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__,static_url_path='/static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# home page
@app.route('/')
def home():
    return render_template('home.html')

'''
List Section
'''
# Component list section
@app.route('/add_com')
def add_comp():
    return render_template('Component/add_component.html')


@app.route('/enter_com', methods=['POST', 'GET'])
def enter_com():
    if request.method == 'POST':
        id_num = request.form['id']
        component_name = request.form['component_name']
        failure_rate = request.form['failure_rate']
        contact = request.form['contact']
        manufacturer = request.form['manufacturer']

        table = "Component"
        data = [id_num, component_name, failure_rate, contact, manufacturer]
        msg = db_query.add_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/list_com')
def list_comp():
    component_data = db_query.list_components(table="Component")
    print(component_data)
    return render_template('Component/list_component.html', rows=component_data)


@app.route('/edit_com')
def edit_comp():
    return render_template('Component/update_component.html')


@app.route('/enter_edit_comp', methods=['POST', 'GET'])
def enter_edit_comp():
    if request.method == 'POST':
        num = request.form['num']
        id_num = request.form['id']
        component_name = request.form['component_name']
        failure_rate = request.form['failure_rate']
        contact = request.form['contact']
        manufacturer = request.form['manufacturer']

        table = "Component"
        data = [id_num, component_name, failure_rate, contact, manufacturer, num]
        msg = db_query.update_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/delete_comp')
def delete_comp():
    return render_template('Component/delete_component.html')


@app.route('/enter_delete_comp', methods=['POST', 'GET'])
def enter_delete_comp():
    if request.method == 'POST':
        id_num = request.form['id']

        table = "Component"

        data = [id_num]
        msg = db_query.delete_components(table, data)
        return render_template("result.html", msg=msg)


# Fail mode list section
@app.route('/add_f_mode')
def add_f_mode():
    return render_template('Fail_mode/add_component.html')


@app.route('/enter_f_mode', methods=['POST', 'GET'])
def enter_f_mode():
    if request.method == 'POST':
        id_num = request.form['id']
        fail_mode = request.form['fail_mode']
        fail_code = request.form['fail_code']

        table = "Fail_mode"
        data = [id_num, fail_mode, fail_code]
        msg = db_query.add_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/list_f_mode')
def list_f_mode():
    fail_mode_data = db_query.list_components(table="Fail_mode")
    return render_template('Fail_mode/list_component.html', rows=fail_mode_data)


@app.route('/edit_f_mode')
def edit_f_mode():
    return render_template('Fail_mode/update_component.html')


@app.route('/enter_edit_f_mode', methods=['POST', 'GET'])
def enter_edit_f_mode():
    if request.method == 'POST':
        num = request.form['num']
        id_num = request.form['id']
        fail_mode = request.form['fail_mode']
        fail_code = request.form['fail_code']

        table = "Fail_mode"
        data = [id_num, fail_mode, fail_code, num]
        msg = db_query.update_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/delete_f_mode')
def delete_f_mode():
    return render_template('Fail_mode/delete_component.html')


@app.route('/enter_delete_f_mode', methods=['POST', 'GET'])
def enter_delete_f_mode():
    if request.method == 'POST':
        id_num = request.form['id']

        table = "Fail_mode"

        data = [id_num]
        msg = db_query.delete_components(table, data)
        return render_template("result.html", msg=msg)


# Mapping list section
@app.route('/add_mapping')
def add_mapping():
    return render_template('Mapping/add_component.html')


@app.route('/enter_mapping', methods=['POST', 'GET'])
def enter_mapping():
    if request.method == 'POST':
        mapping_id_num = request.form['mapping_id']
        component_id = request.form['component_id']
        fail_mode_id = request.form['fail_mode_id']

        table = "Mapping"
        data = [mapping_id_num, component_id, fail_mode_id]
        msg = db_query.add_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/list_mapping')
def list_mapping():
    mapping_data = db_query.list_components(table="Mapping")
    return render_template('Mapping/list_component.html', rows=mapping_data)


@app.route('/edit_mapping')
def edit_mapping():
    return render_template('Mapping/update_component.html')


@app.route('/enter_edit_mapping', methods=['POST', 'GET'])
def enter_edit_mapping():
    if request.method == 'POST':
        num = request.form['num']
        mapping_id = request.form['mapping_id']
        component_id = request.form['component_id']
        fail_mode_id = request.form['fail_mode_id']

        table = "Mapping"
        data = [mapping_id, component_id, fail_mode_id, num]
        msg = db_query.update_components(table, data)
        return render_template("result.html", msg=msg)


@app.route('/delete_mapping')
def delete_mapping():
    return render_template('Mapping/delete_component.html')


@app.route('/enter_delete_mapping', methods=['POST', 'GET'])
def enter_delete_mapping():
    if request.method == 'POST':
        id_num = request.form['mapping_id']
        # mapping_id = request.form['mapping_id']
        # component_id = request.form['component_id']
        # fail_mode_id = request.form['fail_mode_id']

        table = "Mapping"
        # data = [mapping_id, component_id, fail_mode_id, num]
        data = [id_num]
        msg = db_query.delete_components(table, data)
        return render_template("result.html", msg=msg)

'''
Report Section
'''
# report 1
@app.route('/data1', methods=['GET'])
def data1():
    data = sort.report(num=1)
    return make_response(jsonify(data, 200))


@app.route('/report1')
def report1():
    return render_template('Dashboard/report1.html')


# report2
@app.route('/data2', methods=['GET'])
def data2():
    data = sort.report(num=2)
    return make_response(jsonify(data, 200))


@app.route('/report2')
def report2():
    return render_template('Dashboard/report2.html')


# report3
@app.route('/data3', methods=['GET'])
def data3():
    data = sort.report(num=3)
    return make_response(jsonify(data, 200))


@app.route('/report3')
def report3():
    return render_template('Dashboard/report3.html')


'''
Car Net section
'''
@app.route('/upload')
def upload():
    return render_template('upload.html')


@app.route('/upload_file',methods=['GET', 'POST'])   
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return



@app.route('/Upload/<filename>')
def uploaded_file(filename):

    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)



@app.route('/train')
def train():
    # Parameters
    img_size = (128,128)
    batch_size = 10
    lr_rate = 0.001
    num_classes = 2
    epochs = 10

    # Generate dataset
    dataset = gen_data.create_dataset(img_size, batch_size)

    train_ds = dataset[0]
    val_ds = dataset[1]
    
    # Run Car_net model
    time = model.run(img_size,num_classes,lr_rate,epochs,train_ds,val_ds)
    minutes = None
    seconds = None 

    if time >= 60.0:
        minutes = np.round(time / 60.0,3)
        seconds = np.round(time % 60.0,3)
        
    msg = "Done training!"
    msg2 = str(minutes) + 'min' + str(seconds) + 's'

    return render_template('Car_net/train_result.html',msg=msg,msg2=msg2)



@app.route('/predict')
def predict():
    return render_template('Car_net/predict.html')


@app.route('/predict_img',methods=['GET', 'POST'])
def predict_img():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            
            return redirect(url_for('predict_result',filename=filename))
    return

@app.route('/predict_result/<filename>')
def predict_result(filename):
    path = os.getcwd()
    f_path = path + '/Upload/' +filename
    res,Score,duration = test_model.predict(f_path,model_num=3)
    
    msg = res
    msg1 = str(Score)
    msg2 = str(duration) + 's'

    return render_template('Car_net/predict_result.html',msg=msg,msg1=msg1,msg2=msg2)




@app.route('/evaluate')
def evaluate():
    img_size = (128,128)
    batch_size = 10

    dataset = gen_data.create_dataset(img_size,batch_size)
    test_ds = dataset[2]


    # Test for evaluating model (Using model 4)
    loss,acc,duration = test_model.evaluate(test_ds,model_num=3)

    msg = 'Done evaluation!'
    msg1 = str(duration) + 's'
    msg2 = str(loss)
    msg3 = str(acc)


    return render_template('Car_net/evaluate_result.html',msg=msg,msg1=msg1,msg2=msg2,msg3=msg3)




if __name__ == '__main__':
    app.run()
