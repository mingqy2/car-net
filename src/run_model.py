from copy import Error
import matplotlib.pyplot as plt
import numpy as np

# from src import gen_data
import time

import tensorflow as tf
from tensorflow import keras
from tensorflow._api.v2 import experimental
from tensorflow.keras import layers, preprocessing
from tensorflow.keras import models
from tensorflow.keras.models import Sequential
from tensorflow.python.keras import activations
from tensorflow.python.keras.engine.base_layer import Layer
from tensorflow.python.keras.layers.convolutional import Conv, Conv2D
from tensorflow.python.keras.layers.core import Dropout, Flatten
from tensorflow.python.keras.layers.preprocessing.image_preprocessing import RandomContrast, RandomFlip, RandomRotation, RandomZoom

from gen_data import create_dataset


class CAR_NET:
    '''
    Class for Car classifier using CNN
    '''
    def __init__(self,img_size,num_classes,lr_rate,epochs):
        '''
        Default class constructor
        '''
        self.img_size = img_size
        self.num_classes = num_classes
        self.lr_rate = lr_rate
        self.epochs = epochs

        # self.model = None

    def model(self):
        '''
        Build CNN model
        '''

        img_height = self.img_size[0]
        img_width = self.img_size[1]

        # CNN model
        model = Sequential([
            layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height,img_width,3)),
            # Add randomization here to prevent overfitting 
            layers.experimental.preprocessing.RandomFlip("horizontal",input_shape=(img_height,img_width,3)),               
            layers.experimental.preprocessing.RandomRotation(0.1),
            layers.experimental.preprocessing.RandomZoom(0.1),
            layers.experimental.preprocessing.RandomContrast(0.1),
            # CNN architecture
            layers.Conv2D(16,3,padding='same',activation='relu'),
            layers.MaxPooling2D(pool_size=(2,2)),
            layers.Conv2D(32,3,padding='same',activation='relu'),
            layers.MaxPooling2D(pool_size=(2,2)),
            layers.Conv2D(64,3,padding='same',activation='relu'),
            layers.MaxPooling2D(pool_size=(2,2)),
            # Add dropout to prevent overfitting
            layers.Dropout(0.1),
            layers.Flatten(),                                           # 1D array
            layers.Dense(128, activation='relu'),                       # fully connected layers here
            layers.Dense(self.num_classes)

        ])

        # complile the model
        model.compile(optimizer=keras.optimizers.Adam(self.lr_rate),
                    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                    metrics=['accuracy'])
            
        model.summary()


        return model


    def train(self,model,train_ds, val_ds):
        '''
        Train the model
        '''
        history = model.fit(train_ds, validation_data=val_ds, epochs=self.epochs)

        model.save('../Model/model7.h5')

        return history


    def visualize(self,history):
        '''
        Visualize the history report 
        '''

        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']

        loss = history.history['loss']
        val_loss = history.history['val_loss']

        epochs_range = range(self.epochs)

        plt.figure()
        plt.subplots_adjust(hspace=0.5)
        plt.subplot(2,1,1)
        plt.plot(epochs_range,acc,label='Training Accuracy')
        plt.plot(epochs_range,val_acc,label='Validation Accuracy')
        plt.legend()
        plt.title('Training & Validation Accuracy vs Epochs',fontdict={'fontsize':10})
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.grid(True)

        plt.subplot(2,1,2)
        plt.plot(epochs_range,loss,label='Training Loss')
        plt.plot(epochs_range,val_loss,label='Validation Loss')
        plt.legend()
        plt.title('Training & Validation Loss vs Epochs',fontdict={'fontsize':10})
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.grid(True)

        plt.savefig('../static/result.png')



def run(img_size,num_classes,lr_rate,epochs,train_ds,val_ds):
    '''
    Run Car_net Model
    '''


    car_net = CAR_NET(img_size,num_classes,lr_rate=lr_rate,epochs=epochs)
    model = car_net.model()
    start_time = time.time()
    history = car_net.train(model,train_ds,val_ds)
    end_time = time.time()
    car_net.visualize(history)

    duration = end_time-start_time

    return np.round(duration,3)




def main():
    # Parameters
    img_size = (128,128)
    batch_size = 25
    lr_rate = 0.001
    num_classes = 2
    epochs = 10

    # Generate dataset
    dataset = create_dataset(img_size, batch_size)

    train_ds = dataset[0]
    val_ds = dataset[1]

    # Run Car_net model
    run(img_size,num_classes,lr_rate,epochs,train_ds,val_ds)






if __name__ == '__main__':
    try:
        main()
    except:
        raise KeyError

