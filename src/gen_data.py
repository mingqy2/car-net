from genericpath import isdir
import shutil
import matplotlib.pyplot as plt
import os

import tensorflow as tf
from tensorflow import keras
from tensorflow._api.v2 import experimental
from tensorflow.keras import layers, preprocessing
from tensorflow.keras import models
from tensorflow.keras.models import Sequential
from tensorflow.python.data.ops.dataset_ops import AUTOTUNE

import cv2 as cv

# TODO : OpenCV resize images
def add_image_to_dir(image_dir,target_dir=None):
    '''
    Function to move a single image to Test Dir
    
    Input:
    :param img_dir(path): image directory
    :param target_dir(path): target directory

    Output:
    :returns None  
    '''

    # if target_dir == 'train':
    #     target_dir = '/home/dinvincible98/Tesla/ML_project/Data/Train'
    # elif: target_dir == 'test':
    #     target_dir = '/home/dinvincible98/Tesla/ML_project/Data/Test'


    shutil.move(image_dir,target_dir)



def create_dataset(img_size, batch_size):
    '''
    Function for creating dataset from directory

    Input:
    :param img_size(tuple): size of the image (width, height)
    :param batch_size(int): batch size number

    Output:
    :returns dataset(list): list contains all dataset
                            dataset[0]: train_ds
                            dataset[1]: val_ds
                            dataset[2]: test_ds
                            dataset[3]: classes_names
    '''

    # Images directory
    Dir = os.getcwd() 
    data_dir = Dir + '/Data/Train'
    data_dir2 = Dir + '/Data/Test'
    # Parameters
    img_height = img_size[0]
    img_width = img_size[1]

    # Training data set
    train_ds = tf.keras.preprocessing.image_dataset_from_directory(data_dir,
                                                                   validation_split=0.2,
                                                                   subset="training",
                                                                   seed=123,
                                                                   image_size=(img_height, img_width),
                                                                   batch_size=batch_size)
    
    # Validating data set 
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(data_dir,
                                                                   validation_split=0.2,
                                                                   subset="validation",
                                                                   seed=123,
                                                                   image_size=(img_height, img_width),
                                                                   batch_size=batch_size)

    # Test dataset
    test_ds = tf.keras.preprocessing.image_dataset_from_directory(data_dir2,
                                                                seed=123,
                                                                image_size=(img_height, img_width),
                                                                batch_size=batch_size)
    
    class_names = train_ds.class_names
    # print(class_names)

    # class_names2 = test_ds.class_names 
    # print(class_names2)



    # Configure dataset for performance
    AUTOTUNE = tf.data.AUTOTUNE

    train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

    # Standardize the data 
    # Generally, image in rgb value 0-255, we make it small here 0-1
    normalization_layer = layers.experimental.preprocessing.Rescaling(1./255)
    normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
    image_batch, labels_batch = next(iter(normalized_ds))
    first_image = image_batch[0]

    # Notice the pixels values are now in `[0,1]`.
    # print(np.min(first_image), np.max(first_image)) 

    dataset = [train_ds,val_ds,test_ds,class_names] 

    return dataset


def get_classes_names():
    '''
    Get classes_names from directory
    '''
    
    path = os.getcwd()
    # Directory
    Dir = path + '/Data/Train/'

    # Get the directory name
    classes_names = [name for name in os.listdir(Dir) if os.path.isdir(os.path.join(Dir,name))]
    
    classes_names.sort()

    # print(classes_names)

    return classes_names



def visualize_data():
    '''
    Function for visualizing the data
    '''
    
    plt.figure(figsize=(10,10))
    img_size = (128,128)
    batch_size = 10
    train_ds, val_ds,test_ds,class_names = create_dataset(img_size, batch_size)

    for images, labels in train_ds.take(1):
        for i in range(9):
            ax = plt.subplot(3, 3, i+1)
            plt.imshow(images[i].numpy().astype("uint8"))
            plt.title(class_names[labels[i]])
            plt.axis("off")
    
    plt.show()
    
# get_classes_names()