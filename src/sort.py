import numpy as np
import db_query

'''
Quicksort algorithm utils function
'''


def partition(arr, low, high):
    i = low - 1
    pivot = arr[high]
    for j in range(low, high):
        if arr[j] <= pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return i + 1


def quickSort(arr, low, high):
    if len(arr) == 1:
        return arr
    if low < high:
        pi = partition(arr, low, high)
        quickSort(arr, low, pi - 1)
        quickSort(arr, pi + 1, high)
        return arr


'''
function to return key for any value
'''


def get_key(dic, val):
    for key, value in dic.items():
        if val == value:
            return key

    return "key doesn't exist"


'''
Main function for reports
'''


def report(num):
    res = []
    # Top 5 of the most bad quality components
    if num == 1:
        # Get data from component table
        data = db_query.list_components(table="Component")
        # Convert to numpy array
        comp_array = np.array(data)

        # Extract component & fail rate array
        comp_fail_arr = comp_array[:, 2:4]

        # Fail rate array
        failure_rate = comp_fail_arr[:, 1]

        # Map failure rate with corresponding component
        dic = {}
        for c in comp_fail_arr:
            dic[c[1]] = c[0]

        # Store fail rate in a list
        l_failure_rate = failure_rate.tolist()

        # Sort fail rate using quick sort algorithm (small to large)
        n = len(l_failure_rate)
        sorted_list = quickSort(l_failure_rate, 0, n - 1)

        mapping = {}
        # Get top5 failure rate components
        for i in range(n - 1, n - 6, -1):
            # map the sorted value with its corresponding component name
            mapping[dic[sorted_list[i]]] = sorted_list[i]

        res.append(mapping)
        # print(res)

    # Top 3 of the most used test devices
    elif num == 2:
        # Get data from mapping table
        data = db_query.list_components(table="Mapping")
        # Convert to numpy array
        data_array = np.array(data)
        # Extract component id
        comp_ids = data_array[:, 2]

        # Count appearing times of unique component id
        values, count = np.unique(comp_ids, return_counts=True)

        # Map component id with counts
        dic = {}
        n = len(values)
        for i in range(n):
            dic[values[i]] = count[i]

        # Key list
        key_list = list(dic.keys())
        # Value list
        val_list = list(dic.values())

        # Sort count using quick sort algorithm (small to large)
        sorted_list = quickSort(count, 0, n - 1)

        # Final component id list amd value list
        comp_id_list = []
        comp_val_list = []

        # Top 5 most used devices
        for i in range(n - 1, n - 6, -1):
            # Find position of the first index with given value
            pos = val_list.index(sorted_list[i])
            # Append the key with given pos to final comp id list
            comp_id_list.append(key_list[pos])
            # Append the given value to final comp id list
            comp_val_list.append(sorted_list[i])

            # pop first appeared value to avoid duplicates for both val_list and key_list
            val_list.pop(val_list.index(sorted_list[i]))
            key_list.pop(pos)

        # Load component table
        comp_table = db_query.list_components(table="Component")
        comp_array = np.array(comp_table)

        # Extract id and component name array
        id_name = comp_array[:, 1:3]

        # Map id with corresponding component name
        dic2 = {}
        for c in id_name:
            dic2[int(c[0])] = c[1]

        # Map component name with corresponding value
        mapping = {}
        for i in range(len(comp_id_list)):
            mapping[dic2[comp_id_list[i]]] = str(comp_val_list[i])

        res.append(mapping)
        # print(res)

    # Top 5 of the highest PFR manufacturer
    elif num == 3:
        # Get component table
        components = db_query.list_components(table="Component")
        # Convert to numpy array
        comp_array = np.array(components)

        # Extract fail rate array
        failure_rate_arr = comp_array[:, 3]
        # Extract manufacturer array
        manu_arr = comp_array[:, -1]

        # Compose fail rate & manufacturer array
        fail_manu_arr = np.concatenate((np.vstack(failure_rate_arr), np.vstack(manu_arr)), axis=1)

        # Manufacturer list
        manu_list = np.unique(fail_manu_arr[:, -1]).tolist()

        # Map manufacturer with corresponding average PFR
        PFR_dic = {}
        for m in manu_list:
            indics = np.argwhere(fail_manu_arr[:, 1] == m).tolist()
            n = len(indics)
            Sum = 0
            for i in indics:
                Sum += float(fail_manu_arr[i[0]][0])
            ave_PFR = Sum / n

            PFR_dic[m] = ave_PFR

        # Create key list and value list
        key_list = list(PFR_dic.keys())
        val_list = list(PFR_dic.values())

        n = len(key_list)
        # Sort value using quick sort algorithm
        sorted_list = quickSort(val_list, 0, n - 1)

        # Find top3 PFR manufacturer
        mapping = {}
        for i in range(n - 1, n - 4, -1):
            # Get manufacturer using values from PFR_dic
            key = get_key(PFR_dic, sorted_list[i])
            # map key(manufacturer) with sorted value (PFR value in %)
            mapping[key] = str(round(100 * sorted_list[i], 3))

        res.append(mapping)

        # print(res)

    return res


