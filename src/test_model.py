from copy import Error
import tensorflow as tf
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import os
from src import gen_data

import time

# TODO: Return True(>98%) or False 
def predict(image_path, model_num=None):
    '''
    Function for predicting a single images

    Input:
    :param image_path(path): image path
    :param model_num(int): model number
     
    Output:
    :returns res(bool): True is confidence score is >= 98.0 else False

    '''     
    path = os.getcwd() 

    if model_num == 1:
        model_path = path + '/Model/model1.h5'
    elif model_num == 2:
        model_path = path + '/Model/model2.h5'
    elif model_num == 3:
        model_path = path + '/Model/model3.h5'
    else:
        model_path = path + '/Model/model4.h5'

    # Get classes names from directory
    classes_names = gen_data.get_classes_names()

    start_time = time.time()
    
    model = tf.keras.models.load_model(model_path)
    img = tf.keras.preprocessing.image.load_img(image_path,target_size=(128,128))
    img_array = tf.keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch

    prediction = model.predict(img_array)
    score = tf.nn.softmax(prediction[0])
    # print(score)

    label = classes_names[np.argmax(score)]

    Score = 100 * np.max(score)
    
    end_time = time.time()

    duration = np.round(end_time - start_time,3)

    print(f'This image is most likely a {label} with a {Score} confidence score, it takes {duration} s to make prediction')
    
    res = False

    # If the label is car and confidence score >= 98.0, return True
    if label=='Car' and Score >= 98.0:  res = True
    
    return res, Score, duration



def evaluate(test_ds,model_num=None):
    '''
    Function for evaluating a model
    
    Input:
    :param test_ds(tf dataset): test dataset
    :param model_num(int): model number
    
    Output:
    :returns Scores(list): Scores[0] : Loss,Scores[1] : Accuracy
    '''
    path = os.getcwd() 

    if model_num == 1:
        model_path = path + '/Model/model1.h5'
    elif model_num == 2:
        model_path = path + '/Model/model2.h5'
    elif model_num == 3:
        model_path = path + '/Model/model3.h5'
    else:
        model_path = path + '/Model/model4.h5'


    start_time = time.time()

    model = tf.keras.models.load_model(model_path)
    model.summary()

    scores = model.evaluate(test_ds,verbose=2)
    
    end_time = time.time()
    duration = np.round(end_time - start_time,3)

    loss = np.round(scores[0],3)
    acc = np.round(scores[1],3)

    return loss,acc,duration



# def main():

#     img_size = (128,128)
#     batch_size = 32

#     dataset = create_dataset(img_size,batch_size)
#     test_ds = dataset[2]

#     # Test for evaluating model (Using model 4)
#     scores = evaluate(test_ds,model_num=4)

#     print(f'Test loss is {scores[0]}')
#     print(f'Test accuracy is {scores[1]}')

    # classes_names = dataset[3]

    # Test for predicting one image (Using model 3)
    # img_path = '/home/dinvincible98/Tesla/ML_project/Data/Test/None_car/8223949_2928d3f6f6_n.jpg'

    # res,score,duration = predict(img_path,model_num=3)

    # print(res)

    # Test for adding a new image to test dir
    # new_image = '/home/dinvincible98/Tesla/lambo.jpeg' 
    # target_dir = '/home/dinvincible98/Tesla/ML_project/Data/Test/Car'
    # add_image_to_dir(new_image,target_dir)








# if __name__=='__main__':
#     try:
#         main()
#     except:
#         raise Error









# classes = ['Car', 'None_car']
# test_acc_list = []
# test_loss_list = []

# for filename in os.listdir(img_path):
#     if filename.endswith('.jpg'):
#         print(filename)
#         path = img_path + filename
#         # print(path)
#         img = tf.keras.preprocessing.image.load_img(path,target_size=(128,128))

#         img_array = tf.keras.preprocessing.image.img_to_array(img)
#         img_array = tf.expand_dims(img_array, 0) # Create a batch



#         predictions = model.predict(img_array)
#         # print(predictions)
#         score = tf.nn.softmax(predictions[0])


#         # print(
#         #     "This image most likely belongs to {} with a {:.2f} percent confidence."
#         #     .format(classes[np.argmax(score)], 100 * np.max(score))
#         # )

#         # test_loss, test_acc = model.evaluate(img_array,predictions,verbose=2)
#         # test_acc_list.append(test_acc)
#         # test_loss_list.append(test_loss)
    
# print(test_acc_list)

