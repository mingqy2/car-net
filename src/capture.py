from os import error
import cv2 as cv
import os


def capture():
    '''
    Function for capturing image from the webcam
    '''
    Dir = '/home/dinvincible98/Tesla/ML_project/Opencv_img'
    # Change directory
    os.chdir(Dir)
    # define a video capture object
    vid = cv.VideoCapture(0)

    while True:
        ret, frame = vid.read()

        # resized = cv.resize(frame,(640,480),interpolation=cv.INTER_AREA)

        cv.namedWindow('Stream', cv.WINDOW_AUTOSIZE)
        cv.imshow('Stream', frame)

        # cv.namedWindow('Resized',cv.WINDOW_AUTOSIZE)
        # cv.imshow('Resized',resized)

        k = cv.waitKey(1)
        # Press ESC to exit and save the image
        if k == 27:
            cv.imwrite('img1.png', frame)
            break

    vid.release()

    cv.destroyAllWindows()


def main():
    capture()


if __name__ == '__main__':
    try:
        main()
    except:
        raise Exception
